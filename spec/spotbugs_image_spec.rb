require 'tmpdir'
require 'English'

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def image_name
    ENV.fetch('TMP_IMAGE', 'spotbugs:latest')
  end

  def target_mount_dir
    '/app'
  end

  context 'with no project' do
    before(:context) do
      @output = `docker run -t --rm -w #{target_mount_dir} #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it 'shows there is no match' do
      expect(@output).to match(/no match in #{target_mount_dir}/i)
    end

    describe 'exit code' do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context 'with test project' do
    def parse_expected_report(expectation_name)
      path = File.join(expectations_dir, expectation_name, 'gl-sast-report.json')
      JSON.parse(File.read(path))
    end

    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',
        # CI_PROJECT_DIR is needed for `post-analyzers/scripts` to
        # properly resolve file locations
        # https://gitlab.com/gitlab-org/security-products/post-analyzers/scripts/-/blob/25479eae03e423cd67f2493f23d0c4f9789cdd0e/start.sh#L2
        'CI_PROJECT_DIR': target_mount_dir,
        'SEARCH_MAX_DEPTH': 20
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json')
    end

    let(:report) { scan.report }

    context 'when using kotlin with gradle build tool' do
      let(:project) { 'kotlin' }

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end
    end

    context 'when using groovy with gradle build tool' do
      let(:project) { 'groovy' }

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end
    end

    context 'when using mono-repository containing scala, kotlin and groovy language files' do
      let(:project) { 'monorepo' }

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end
    end

    context 'when using scala with SBT build tool' do
      let(:project) { 'scala' }

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end
    end

  end
end
