// Package convert translates a SpotBugs XML report into a issue.Report.
package convert

import (
	"encoding/xml"
	"io"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2/instance"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2/metadata"
)

// Convert translate a SpotBugs XML report into a issue.Report.
func Convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var doc = struct {
		BugInstances []instance.Instance `xml:"BugInstance"`
	}{}

	err := xml.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	vulns := make([]report.Vulnerability, len(doc.BugInstances))
	for i, bug := range doc.BugInstances {
		vulns[i] = report.Vulnerability{
			Category:    metadata.Type,
			Scanner:     &metadata.IssueScanner,
			Name:        bug.ShortMessage,
			Message:     bug.ShortMessage,
			Description: bug.LongMessage, // Could be extracted from BugPattern/Details instead
			CompareKey:  bug.CompareKey(),
			Severity:    bug.Severity(),
			Confidence:  bug.Confidence(),
			// Solution: bug.Solution(), Need to parse BugPattern/Details to extract solution
			Location:    bug.Location(prependPath),
			Identifiers: bug.Identifiers(),
			// Links:    bug.Links(), Need to parse BugPattern/Details to extract links
		}
	}

	var newReport = report.NewReport()
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = vulns
	return &newReport, nil
}
