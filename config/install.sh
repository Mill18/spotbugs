#!/bin/bash
set -euo pipefail

echo ["$(date "+%H:%M:%S")"] "==> Installing packages…"

apk add \
  curl \
  zip \
  zstd \
  xz \
  git \
  alpine-sdk \
  autoconf \
  ncurses \
  ncurses-dev

# glibc
curl -LSs https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub \
    && curl -LSs "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/$GLIBC_VERSION/glibc-$GLIBC_VERSION.apk" > /tmp/glibc.apk \
    && apk add /tmp/glibc.apk \
    && rm /tmp/glibc.apk

# zlib
curl -LSs "https://archive.archlinux.org/packages/z/zlib/zlib-$ZLIB_VERSION.pkg.tar.xz" -o /tmp/zlib.tar.xz \
    && echo "$ZLIB_SHA1SUM  /tmp/zlib.tar.xz" | sha1sum -c \
    && mkdir -p /tmp/zlib && tar xvf /tmp/zlib.tar.xz -C /tmp/zlib \
    && cp /tmp/zlib/usr/lib/libz.* /usr/glibc-compat/lib/ \
    && rm -rf /tmp/zlib /tmp/zlib.tar.xz

# gcc libs
apk add --no-cache binutils \
    && curl -LSs "https://archive.archlinux.org/packages/g/gcc-libs/gcc-libs-$GCC_LIBS_VERSION.pkg.tar.zst" -o /tmp/gcc-libs.tar.zst \
    && echo "$GCC_LIBS_SHA1SUM  /tmp/gcc-libs.tar.zst" | sha1sum -c \
    && mkdir -p /tmp/gcc-libs \
    && zstd -d /tmp/gcc-libs.tar.zst && tar xvf /tmp/gcc-libs.tar -C /tmp/gcc-libs \
    && mv /tmp/gcc-libs/usr/lib/libgcc* /tmp/gcc-libs/usr/lib/libstdc++* /usr/glibc-compat/lib \
    && strip /usr/glibc-compat/lib/libgcc_s.so.* \
    && strip /usr/glibc-compat/lib/libstdc++.so.* \
    && rm -rf /tmp/gcc-libs /tmp/gcc-libs.tar.xz \
    && apk del binutils

echo ["$(date "+%H:%M:%S")"] "==> Installing asdf…"
mkdir -p "$ASDF_DATA_DIR"
git clone https://github.com/asdf-vm/asdf.git "$ASDF_DATA_DIR" --branch "$ASDF_VERSION"

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/asdf.sh
asdf plugin add ant https://gitlab.com/theoretick/asdf-ant
asdf plugin add java
asdf plugin add gradle
asdf plugin add grails
# TODO: Use official asdf-maven plugin in gemnasium-maven
# See https://gitlab.com/gitlab-org/gitlab/-/issues/224142
asdf plugin add maven https://github.com/adamcohen/asdf-maven
asdf plugin add sbt
asdf plugin add scala

asdf install
asdf reshim
asdf current

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/java/set-java-home.bash
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/ant/set-ant-home.bash

# Ensure that Java is working correctly by printing the version.
asdf list java | while read -r jdk_version ; do
  java_bin="$(asdf where java "$jdk_version")/bin/java"
  $java_bin -version 2&> /dev/null
  exit_code=$?
  if [ $exit_code -ne 0 ]; then
      echo "Java installation check failed. Attempted to run '$java_bin -version' and got exit code $exit_code" && exit 1
  fi
done;

# Install SpotBugs CLI
cd /spotbugs && \
  mkdir -p dist && \
  wget "https://repo.maven.apache.org/maven2/com/github/spotbugs/spotbugs/${SCANNER_VERSION}/spotbugs-${SCANNER_VERSION}.tgz" && \
  tar xzf "spotbugs-${SCANNER_VERSION}.tgz" -C dist --strip-components 1 && \
  rm -f "spotbugs-${SCANNER_VERSION}.tgz"

# Install FindSecBugs for use as a SpotBugs plugin
mkdir -p /fsb && \
  cd /fsb && \
  wget "https://github.com/find-sec-bugs/find-sec-bugs/releases/download/version-${FINDSECBUGS_VERSION}/findsecbugs-cli-${FINDSECBUGS_VERSION}.zip" && \
  unzip -n "findsecbugs-cli-${FINDSECBUGS_VERSION}.zip" && \
  rm -f "findsecbugs-cli-${FINDSECBUGS_VERSION}.zip" && \
  mv "lib/findsecbugs-plugin-${FINDSECBUGS_VERSION}.jar" lib/findsecbugs-plugin.jar

echo ["$(date "+%H:%M:%S")"] "==> Beginning cleanup…"

rm -fr \
  "$ASDF_DATA_DIR/docs" \
  "$ASDF_DATA_DIR"/installs/java/**/demo \
  "$ASDF_DATA_DIR"/installs/java/**/man \
  "$ASDF_DATA_DIR"/installs/java/**/sample \
  "$ASDF_DATA_DIR"/installs/grails/**/media \
  "$ASDF_DATA_DIR"/installs/scala/**/doc \
  "$ASDF_DATA_DIR"/installs/scala/**/man \
  "$ASDF_DATA_DIR"/installs/**/**/share \
  "$ASDF_DATA_DIR"/test

echo ["$(date "+%H:%M:%S")"] "==> Done"
